import pandas as pd
import numpy as np
import importlib
import math
from sklearn.preprocessing import PolynomialFeatures
from sklearn.utils import check_array


class Forecast:
    
    def __init__(self, t, Y, window_size=7, regression_model='sklearn.linear_model.BayesianRidge', optimize=False, experience=None):
                
        # Importing modules for model
        self.model = regression_model.split('.')[-1]
        to_import = regression_model.replace('.'+ self.model, '')
        self.module = importlib.import_module(to_import)
        self.regression_model = getattr(self.module, self.model)()

        self.pol_model = PolynomialFeatures(degree=3, interaction_only=True)
        self.window_size = window_size
        self.t = t

        ma_yt = self.__MA_yt(Y)
        st_it = self.__St_It(Y, ma_yt)
        st, self.st_condensed = self.__St(st_it)
        
        if experience != None:
            self.st_condensed = [(self.st_condensed[i] + experience[i])/2 for i in range(0, len(experience))]
            st = [0] * len(st_it)
            for i in range(0,self.window_size):
                for j in range(i, len(st_it), self.window_size):
                    st[j] = self.st_condensed[i]

        
        dst = self.__DSt(Y, st)

        t = [[t[i]] for i in range(0, len(t))]
        t = self.pol_model.fit_transform(t, dst)
        if not optimize:
            # train model
            self.regression_model.fit(t, dst)
        else:
            validation = math.ceil(len(t)*.2)
            t_train, y_train, t_test, y_test = test_train_split(t, dst, validation)
            
            self.regression_model = getattr(self.module, self.model)()
            self.regression_model.fit(t_train, y_train)
            
            max_val = int(len(t_train)/window_size) + 1
            best_performance = mean_absolute_percentage_error(y_test, self.regression_model.predict(t_test))
            best_train = 0
            for i in range(1, max_val):
                self.regression_model = getattr(self.module, self.model)()
                self.regression_model.fit(t_train[-1*window_size*i:], y_train[-1*window_size*i:])
                
                current_performance = mean_absolute_percentage_error(y_test, self.regression_model.predict(t_test))
                if current_performance < best_performance:
                    best_performance = current_performance
                    best_train = i
                else:
                    break
            
            self.t = t[-1*window_size*best_train:]
            self.dst = dst[-1*window_size*best_train:]
            self.best_trend = best_train
            self.regression_model = getattr(self.module, self.model)()
            self.regression_model.fit(self.t, self.dst)
            


    # Moving Average
    def __MA_yt(self, Y):
        ma_yt = []
        odd = True if self.window_size%2 != 0 else False

        if odd:
            lims = int((self.window_size - 1)/2)
            ma_yt = [0] * lims
            for i in range(0, len(Y) - self.window_size + 1):
                ma_yt = ma_yt + [sum(Y[i : i + self.window_size]) / float(self.window_size)]
            ma_yt = ma_yt + [0] * lims
        return ma_yt

    def __St_It(self, Y, MAyt):
        st_it = []
        for i in range(0, len(Y)):
            if MAyt[i] == 0:
                st_it = st_it + [0]
                continue
            st_it = st_it + [Y[i]/MAyt[i]]
        return st_it
    
    def __St(self, st_it):
        odd = True if self.window_size%2 != 0 else False
        if odd:
            lims = int((self.window_size - 1)/2)
        low_limit =  lims
        upper_limit = len(st_it) - lims + 1
        st_values = []
        st_condensed = []
    
        for i in range(0,self.window_size):
            values = []
            for j in range(i, len(st_it), self.window_size):
                if j < low_limit or j >= upper_limit:
                    continue
                else:
                    if st_it[j] != 0:
                        values.append(st_it[j])
            st_mean = np.mean(values)
            st_values.append(st_mean)
            st_condensed.append(st_mean)
    
        st = [0] * len(st_it)
        for i in range(0,self.window_size):
            for j in range(i, len(st_it), self.window_size):
                if st[j] != 0: print(st[j])
                st[j] = st_values[i]
        return  st, st_condensed

    def __DSt(self, Y, st):
        dst = [0]*len(st)
        for i in range(0, len(st)):
            dst[i] = Y[i]/st[i]
        return dst
    
    def __predict(self, t, optimize):
        t = [[t[i]] for i in range(0, len(t))]
        if not optimize:
            t = self.pol_model.transform(t)
            prediction = self.regression_model.predict(t)
            return prediction
        else:
            sub_t = [t[i:i+self.window_size] for i in range(0, len(t), self.window_size)]
            _t = list(self.t)
            _dst = list(self.dst)
            transform = self.pol_model.transform(sub_t[0])
            pred = self.regression_model.predict(transform)
            _dst += list(pred)
            _t += list(transform)            
            prediction = list(pred)
            for i in range(1, len(sub_t)):
                regression_model = getattr(self.module, self.model)()
                regression_model.fit(_t[-1*self.window_size*self.best_trend::5], _dst[-1*self.window_size*self.best_trend::5])
                transform = self.pol_model.transform(sub_t[i])
                pred = regression_model.predict(transform)
                _t += list(transform)
                _dst += list(pred)
                prediction += list(pred)
            return prediction
            
    def predict(self, t, optimize=False):
        Tt = self.__predict(t, optimize=optimize)
        prediction = []
        
        for i in range(0, len(t)):
            st = self.window_size-1 if t[i]%self.window_size == 0 else t[i]%self.window_size-1
            prediction.append(self.st_condensed[st] * Tt[i])
        return prediction

    
    
def to_integer(dt_time):
    return 10000*dt_time.year + 100*dt_time.month + dt_time.day

# dates, stream = get_song("rockstar", 'no')
def get_song(song, region):
    df = pd.read_csv('data/spotify/data.csv', index_col=None)
    region_df = df.loc[df['Region'] == region]
    df_song = region_df.loc[df['Track Name'] == song]
    df_song['Streams'] = df_song['Streams'].astype('int64')
    df_song['Date'] = df_song['Date'].astype('datetime64[ns]')
    dates = df_song['Date'].tolist()
    dates = (list(pd.to_datetime(df_song['Date']).apply(lambda x: x.date())))
    streams = df_song['Streams'].tolist()
    positions = df_song['Position'].tolist()
    return dates, streams

def test_train_split(x, y, test_size = 14):
    x_train = x[:-test_size]
    x_test  = x[-test_size:]
    y_train = y[:-test_size]
    y_test  = y[-test_size:]
    return x_train, y_train, x_test, y_test

def dummy(x):
    return [x for x in range(1, len(x)+1)]

def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100